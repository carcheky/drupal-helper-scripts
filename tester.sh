#!/bin/bash

# function create CURRENT_BRANCH var
function get_current_branch() {
    CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
}

# function create and push empty commit
function empty_commit() {
    sleep 70
    get_current_branch
    git commit --allow-empty -m "fix: empty commit on $CURRENT_BRANCH"
    git push --set-upstream origin $CURRENT_BRANCH
}

# function create and push fix commit
function fix_commit() {
    sleep 70
    get_current_branch
    git commit --allow-empty -m "fix: fix commit on $CURRENT_BRANCH"
    git push --set-upstream origin $CURRENT_BRANCH
}

# function create and push feat commit
function feat_commit() {
    sleep 70
    get_current_branch
    git commit --allow-empty -m "feat: feat commit on $CURRENT_BRANCH"
    git push --set-upstream origin $CURRENT_BRANCH
}

# function create and push !BREAKING CHANGE:
function breaking_commit() {
    sleep 70
    get_current_branch
    git commit --allow-empty -m "feat: feat commit on $CURRENT_BRANCH

!BREAKING CHANGE: breaking commit"
    git push --set-upstream origin $CURRENT_BRANCH
}

# function fully delete master branch history
function delete_master_history() {
    get_current_branch

    git add .
    git commit -m "all"
    git checkout --orphan latest_branch
    git add -A
    git commit -am "feat: first version"
    git branch -D $CURRENT_BRANCH
    git branch -m $CURRENT_BRANCH
    git push -f origin $CURRENT_BRANCH
    git branch --set-upstream-to=origin/$CURRENT_BRANCH master
}

# function force delete and create from master $1
function force_propagate_master() {
    sleep 70
    get_current_branch
    git checkout master
    git branch -D $CURRENT_BRANCH
    git checkout -b $CURRENT_BRANCH
    git push origin $CURRENT_BRANCH --force
}

# function delete all tags
function delete_all_tags() {
    git tag | xargs git tag -d
    git push origin --delete $(git tag -l)
}

delete_all_tags
delete_master_history
# fix_commit
# force_propagate_master 'next'
# fix_commit
# force_propagate_master 'next-major'
# feat_commit
# force_propagate_master '1.x'
# fix_commit
# force_propagate_master '2.x'
# breaking_commit
git pull
